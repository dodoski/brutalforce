package com.example.demo.service;

import com.example.demo.repository.UserRepository;

public class UserService {

    private final UserRepository userRepository = new UserRepository();

    public boolean checkLogin(String login, String password) {
        return this.userRepository.checkLogin(login, password);
    }

    public boolean checkActivateUserLogin(String login) { return this.userRepository.checkActivateUserLogin(login);}
}
